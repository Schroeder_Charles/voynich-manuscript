/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schroeder_voynich_langauge_generator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import static java.lang.System.exit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Charlie
 */
public class UserInterface extends JFrame {
 
    //defining Jwindow parameters
	JButton openButton = new JButton("Encrypt a file");
	JButton cancelButton = new JButton("Cancel");
        JButton examineAlphabet = new JButton("Examine Alphabets");
        JButton generateAlphabet = new JButton("Generate Alphabet");
	static JTextArea textarea = new JTextArea();
	JScrollPane outputfield = new JScrollPane(textarea);
        //JTextArea inputMod = new JTextArea();
        
        char [] Alphabet = LanguageGenerator.languageGenerator();
        
        int mod = 29;
        
        public UserInterface()
        {
            //general window properties
		this.setTitle("Language Generator");
		this.setBounds(200, 200, 600, 400);
		
		//setting layout manager to fully manual
		this.getContentPane().setLayout(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//textfield settings
		this.getContentPane().add(outputfield);
		this.outputfield.setBounds(30, 30, 540, 300);
		this.textarea.setBounds(30, 30, 500, 300);
		this.textarea.setEditable(false);
		//this.textarea.append("Please choose a file." + "\n");
		//this.textarea.append("Each word and its number of uses will appear here:" + "\n");
                
		//buttons
		this.openButton.setBounds(25, 338, 150, 30);
		this.getContentPane().add(openButton);
		this.openButton.addActionListener( new OpenButtonListener());
                
                this.generateAlphabet.setBounds(200, 338, 150, 30);
                this.getContentPane().add(generateAlphabet);
                this.generateAlphabet.addActionListener(new GenerateButtonListener());
                
                this.cancelButton.setBounds(480, 338, 100, 30);
                this.getContentPane().add(cancelButton);
                this.cancelButton.addActionListener(new CancelButtonListener());
        }
        
         /**
    This class and function handle user input on the buttons in the GUI. The ActionEvent of
    pressing the button is passed in, and the chosen file is checked, and if it is a text file,
    approved. This file is then passed to the WordCounterHandler class.
    @param ActionEvent
     */
    private class OpenButtonListener implements ActionListener {
        @Override
        public void actionPerformed( ActionEvent e) {
            
        	//creating filechooser, setting acceptable file types
            JFileChooser chooser = new JFileChooser();
            chooser.setFileFilter( new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
            
            int chooserSuccess = chooser.showOpenDialog( null);
         
            if( chooserSuccess == JFileChooser.APPROVE_OPTION) 
            {
                File chosenFile = chooser.getSelectedFile();                
                try {
                    LanguageGenerator.encrypt(Alphabet, mod, chosenFile);
                } catch (IOException ex) {
                    Logger.getLogger(UserInterface.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public static void writeToGUI(char text [])
    {
    	//outputs the word and its count to the GUI
        //textarea.append("\"");
        
        for(int i =0; i < text.length; i++)
        {
             textarea.append( text[i] + " ");
        }
        //textarea.append("\"" + "," + "\n");
        textarea.append("\n");
    }
    
    private class GenerateButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            char [] newAlphabet = new char [Alphabet.length];
            newAlphabet = LanguageGenerator.generateNewAlphabet(Alphabet, mod);
            writeToGUI(newAlphabet);
        }
    }
    
    private class CancelButtonListener implements ActionListener {
  
        @Override
        public void actionPerformed(ActionEvent e) {
            exit(0);
        }
    }
    
}
