/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schroeder_voynich_langauge_generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Charlie
 */
public class LanguageGenerator {
    
    public static char [] languageGenerator()
    {
           
        char initialAlphabet [] = new char [24];
    
        initialAlphabet[0] = 'a';
        initialAlphabet[1] = 'b';
        initialAlphabet[2] = 'c';
        initialAlphabet[3] = 'd';
        initialAlphabet[4] = 'e';
        initialAlphabet[5] = 'f';
        initialAlphabet[6] = 'g';
        initialAlphabet[7] = 'h';
        initialAlphabet[8] = 'i';
        initialAlphabet[9] = 'k';
        initialAlphabet[10] = 'l';
        initialAlphabet[11] = 'm';
        initialAlphabet[12] = 'n';
        initialAlphabet[13] = 'o';
        initialAlphabet[14] = 'p';
        initialAlphabet[15] = 'q';
        initialAlphabet[16] = 'r';
        initialAlphabet[17] = 's';
        initialAlphabet[18] = 't';
        initialAlphabet[19] = 'v';
        initialAlphabet[20] = 'x';
        initialAlphabet[21] = 'y';
        initialAlphabet[22] = 'z';
        //initialAlphabet[23] = '';
        
        return(initialAlphabet);
    }
    
    
    public static void examinePossibleAlphabets(char [] inputAlphabet, int mod)
    {
        char [] proxyAlphabet = inputAlphabet;
        char [] newAlphabet = new char [23];
        
        for(int i = 0; i < mod; i++)
        {
            System.out.println("__________");
            
            int totalCipherLength = 0;
            int [] countableLetters = new int [24];
            int count;
            count = 0;
            int totalCount;
            totalCount = 0;
            
            for(int j =1; j < 24; j++)
            {
               //System.out.println(inputAlphabet[j] + " " + i % j);
               
               countableLetters[j-1] = i % j;
               
               totalCount =0;
               for(int p =0; p < 24; p++)
               {
                   count = 0;
                   for(int q =0; q < 23; q++)
                   {
                       if(countableLetters[q] == p)
                       {
                           count++;
                       }
                   }
                   
                   if(count > 0)
                       {
                           totalCount++;
                       }
               }
               
            }
            
            System.out.println("When n = " + i + " total Unique= " + totalCount);
        }
    }
    
    
    public static char [] generateNewAlphabet(char [] inputAlphabet, int mod)
    {
        char [] newAlphabet = new char [23];
            
            int totalCipherLength = 0;
            int [] countableLetters = new int [24];
            int count;
            count = 0;
            int totalCount;
            totalCount = 0;
            
            for(int j=1; j < 24; j++)
            {
               newAlphabet[j-1] = inputAlphabet[mod % j];   
            }
         
            return newAlphabet;
    }
    
    
    public static void encrypt(char [] Alphabet, int mod, File fileName) throws FileNotFoundException
    {
        Scanner sc = new Scanner(fileName);
        
        while(sc.hasNext())
        {
            String currentWord = sc.next();
            int wordLength = currentWord.length();
            char [] cipheredWord = new char [wordLength];
            char currentLetter;
            int index = 0;
            
            for(int i =0; i < wordLength; i++)
            {
                currentLetter = currentWord.charAt(i);
                while(currentLetter != Alphabet[index] && index < 23)
                {
                    index++;
                }
                
                cipheredWord[i] = Alphabet[mod % (index + 1)];
                index = 0;
            }
            
            UserInterface.writeToGUI(cipheredWord);
            
        }
    }
    
}
